#include "config.h"

#define _GNU_SOURCE /* Required for CLONE_NEWNS */
#include <sys/mount.h>
#include <sched.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#if 0
#define __debug__(x) printf x
#else
#define __debug__(x)
#endif

char *
strconcat (const char *s1,
	   const char *s2,
	   const char *s3)
{
  size_t len = 0;
  char *res;

  if (s1)
    len += strlen (s1);
  if (s2)
    len += strlen (s2);
  if (s3)
    len += strlen (s3);

  res = malloc (len + 1);
  if (res == NULL)
    return NULL;

  *res = 0;
  if (s1)
    strcat (res, s1);
  if (s2)
    strcat (res, s2);
  if (s3)
    strcat (res, s3);

  return res;
}

static void
update_env_var_list (const char *var, const char *item)
{
  const char *env;
  char *value;

  env = getenv (var);
  if (env == NULL || *env == 0)
    {
      setenv (var, item, 1);
    }
  else
    {
      value = strconcat (item, ":", env);
      setenv (var, value, 1);
      free (value);
    }
}

#ifndef MS_PRIVATE      /* May not be defined in older glibc headers */
#define MS_PRIVATE (1<<18) /* change to private */
#endif

int
main (int argc,
      char **argv)
{
  int res;
  char *mount_source;
  char *executable_relative;
  char *executable;
  char *extra_mount_source;
  char **child_argv;
  int i, j, fd, argv_offset;
  int mount_count;
  int use_tmpfs;

  /* The initial code is run with a high permission euid
     (at least CAP_SYS_ADMIN), so take lots of care. */

  if (argc < 3) {
    fprintf (stderr, "Not enough arguments. Need source dir and executable relative path.\n");
    return 1;
  }

  argv_offset = 1;
  mount_source = argv[argv_offset++];
  executable_relative = argv[argv_offset++];

  fd = 0;
  extra_mount_source = 0;

  while (argv_offset + 1 < argc)
    {
      if (strcmp (argv[argv_offset], "-fd") == 0)
	fd = atoi (argv[argv_offset+1]);
      else if (strcmp (argv[argv_offset], "-extra") == 0)
	extra_mount_source = argv[argv_offset+1];
      else
	break;

      argv_offset += 2;
    }

  __debug__(("creating new namespace\n"));
  res = unshare (CLONE_NEWNS);
  if (res != 0) {
    perror ("Creating new namespace failed");
    return 1;
  }

  __debug__(("mount bundle (private)\n"));
  mount_count = 0;
  res = mount (BUNDLE_PREFIX, BUNDLE_PREFIX,
	       NULL, MS_PRIVATE, NULL);
  if (res != 0 && errno == EINVAL) {
    /* Maybe if failed because there is no mount
       to be made private at that point, lets
       add a bind mount there. */
    __debug__(("mount bundle (bind)\n"));
    res = mount (BUNDLE_PREFIX, BUNDLE_PREFIX,
		 NULL, MS_BIND, NULL);
    /* And try again */
    if (res == 0)
      {
	mount_count++; /* Bind mount succeeded */
        __debug__(("mount bundle (private)\n"));
	res = mount (BUNDLE_PREFIX, BUNDLE_PREFIX,
		     NULL, MS_PRIVATE, NULL);
      }
  }

  if (res != 0) {
    perror ("Failed to make prefix namespace private");
    goto error_out;
  }

  if (extra_mount_source != NULL)
    {
      __debug__(("mount extra source (private)\n"));
      res = mount (extra_mount_source, extra_mount_source,
	     NULL, MS_PRIVATE, NULL);
      if (res != 0) {
	perror ("Failed to bind the extra source directory");
	goto error_out;
      }

      __debug__(("mount extra source (bind)\n"));
      res = mount (extra_mount_source, extra_mount_source,
		   NULL, MS_BIND, NULL);
      if (res != 0) {
	perror ("Failed to bind the extra source directory");
	goto error_out;
      }
    }

  use_tmpfs = 0;
  __debug__(("mount source %s to %s\n", mount_source, BUNDLE_PREFIX));
  res = mount (mount_source, BUNDLE_PREFIX,
	       NULL, MS_BIND, NULL);
  if (res != 0) 
    {
      int errsv = errno;
      if (errsv == EACCES)
	{
	  /* Some older kernels don't allow bind mounting (as root) a file
	     inside a (non-root) fuse mount (as the uid differs). For these
	     cases we create a private tmpfs mount with a symlink in */
	  res = mount ("tmpfs" , BUNDLE_PREFIX,
		       "tmpfs", MS_NODEV|MS_NOEXEC, NULL);
	  
	  if (res != 0)
	    {
	      perror ("Failed to mount tmpfs");
	      goto error_out;
	    }

	  use_tmpfs = 1;
	}
      else
	{
	  perror ("Failed to bind the source directory");
	  goto error_out;
	}
    }
  mount_count++; /* Normal mount succeeded */

  /* Now we have everything we need CAP_SYS_ADMIN for, so drop setuid */
  setuid (getuid ());

  executable = NULL;
  child_argv = NULL;

  if (use_tmpfs)
    {
      char *mount_source_data = strconcat (mount_source, "/data", NULL);
      if (mount_source_data == NULL)
	goto oom;
      res = symlink (mount_source_data, BUNDLE_PREFIX "/data");
      if (res != 0)
	{
	  perror ("Unable to create symlink");
	  free (mount_source_data);
	  goto error_out;
	}
      free (mount_source_data);
    }

  if (fd != 0)
    {
      char c = 'x';
      write (fd, &c, 1);
    }

  if (executable_relative[0] == '/')
    executable = executable_relative;
  else
    {
      executable = malloc (strlen (BUNDLE_PREFIX "/data") + strlen (executable_relative) + 1);
      if (executable != NULL)
	{
	  strcpy (executable, BUNDLE_PREFIX "/data");
	  strcat (executable, executable_relative);
	}
    }

  if (executable == NULL)
    goto oom;

  child_argv = malloc ((1 + argc - argv_offset + 1) * sizeof (char *));
  if (child_argv == NULL)
    goto oom;

  j = 0;
  for (i = argv_offset; i < argc; i++)
    child_argv[j++] = argv[i];
  child_argv[j++] = NULL;

  update_env_var_list ("LD_LIBRARY_PATH", BUNDLE_PREFIX "/data/lib");

  __debug__(("launch executable %s\n", executable));
  return execv (executable, child_argv);

 oom:
  if (executable)
    free (executable);

  fprintf (stderr, "Out of memory.\n");

 error_out:
  while (mount_count-- > 0)
    umount (BUNDLE_PREFIX);
  return 1;
}
