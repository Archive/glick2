%if 0%{?fedora} < 15 && 0%{?rhel} <= 6
%define use_binfmt_d 0
%else
%define use_binfmt_d 1
%endif

Name:		glick2
Version:	0.0.4
Release:	1%{?dist}
Summary:	An application bundle runtime

License:	GPLv2+
URL:		http://people.gnome.org/~alexl/glick2/
Source0:	http://people.gnome.org/~alexl/glick2/releases/glick2-%{version}.tar.gz

BuildRequires:	glib2-devel, fuse-devel

%description
Glick2 is a runtime and a set of tools to create
application bundles for Linux. An application bundle is a single file
that contains all the data and files needed to run an application. The
bundle can be run without installation, or be installed by just
putting the file in a known directory.

%package gnome-session
Summary:        Integration with the gnome session
Group:          System Environment/Libraries
License:        GPLv2+
Requires:       gnome-session

%description gnome-session
Adds a glick integrated gnome session

%package tools
Summary:        Tools to create glick2 bundles
Group:          System Environment/Libraries
License:        GPLv2+

%description tools
This package contains tools needed to create glick2
bundles.

%prep
%setup -q

%build
%configure --disable-setuid-install
make V=1 %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/xsessions
install gnome-glick2.desktop $RPM_BUILD_ROOT/%{_datadir}/xsessions
install gnome-glick-session $RPM_BUILD_ROOT/%{_bindir}

# Check for pre-systemd OSes and switch to sysv init for binfmt registration
%if !%{use_binfmt_d}
mkdir -p  $RPM_BUILD_ROOT/%{_sysconfdir}/init.d/
install glick2.init $RPM_BUILD_ROOT/%{_sysconfdir}/init.d/glick2
rm -rf $RPM_BUILD_ROOT/%{_sysconfdir}/binfmt.d
%endif


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README
%{_bindir}/glick-fs
%{_bindir}/glick-runner
%attr(4755,root,root) %{_bindir}/glick-session
%attr(4755,root,root) %{_libexecdir}/glick-helper
%{_datadir}/mime/packages/glick2.xml
%if %{use_binfmt_d}
%{_sysconfdir}/binfmt.d/glick2.conf
%else
%{_sysconfdir}/init.d/glick2
%endif
%{_sysconfdir}/xdg/autostart/glick.desktop
%dir /opt/bundle
%dir /opt/session

%post
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :

%if %{use_binfmt_d}
/bin/systemctl --system try-restart systemd-binfmt.service &>/dev/null || :
%else
/sbin/chkconfig --add glick2
%endif

%preun
%if !%{use_binfmt_d}
if [ $1 -eq 0 ]; then
   /sbin/service glick2 stop &>/dev/null || :
   /sbin/chkconfig --del glick2
fi
%endif

%postun
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :
%if %{use_binfmt_d}
/bin/systemctl --system try-restart systemd-binfmt.service &>/dev/null || :
%else
if [ $1 -ge 1 ]; then
   /sbin/service glick2 condrestart &>/dev/null || :
fi
%endif
   
%files tools
%{_bindir}/glick-mkbundle

%files gnome-session
%{_datadir}/xsessions/gnome-glick2.desktop
%{_bindir}/gnome-glick-session

%changelog
* Thu Oct 13 2011 Alexander Larsson <alexl@redhat.com> - 0.0.1-1
- Initial version

